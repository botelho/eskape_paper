# ESKAPE_paper

This is the code repository for reproducing the main figures in the paper:

[The ESKAPE mobilome contributes to the spread of antimicrobial resistance and CRISPR-mediated conflict between mobile genetic elements](https://www.biorxiv.org/content/10.1101/2022.01.03.474784v1.full)
